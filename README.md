# ToDo App
ToDoApp scritta in Kotlin per il corso di [PROGRAMMAZIONE DI DISPOSITIVI MOBILI A.A. 2019/2020](https://www.uninsubria.it/ugov/degreecourse/117148).

## TAGs
*  **v1.0**: In questa prima versione l'App contine una sola Activity con una ListView per aggiungere o cancellare degli oggetti TodoItem. Tutto viene salvato in memoria
*  **v2.0**: l'App usa un database locale SQLite per salvare i dati, ripristinarli, aggiungere e cancellare.
*  **v3.0**: 
   * Introdotta la ToolBar al posto della ActionBar di default.
   * Aggiunti menu, icona, titolo e altro nella ToolBar.
   * Introdotta una nuova Activity per l'inserimento di una nuova TodoItem.
*  **v4.0**: Aggiunta una CircularProgessBar custom view alla ListView per mostrare la quantità di tempo trascorso dalla creazione del TodoItem fino alla data di scadenza del TodoItem.
   * Modificata anche la Activity per l'inserimento della nuova Todo per aggiungere la data di scadenza.
   * Aggiunto un campo nel DB per la nuova data di deadline.
*  **v5.0**: Utilizzo dei Thread per scaricare immagine da web e aggiungere nella GUI.
   * Aggiunto un nuovo campo alla classe TodoItem per memorizzare la URL di un'immagine da associare al TodoItem.
   * Modificato il DB e le due Activity
*  **v6.0**: Aggiunta autenticazione tramite Firebase
   * LoginActivity per poter accedere tramite email e password
   * SignUpActivity per poter registrare un nuovo utente su Firebase
   
 ## Visione d'insieme delle Activity che compongono l'App.
 ![designe ToDo App](https://gitlab.com/ignazio.gallo/todoapp-kotlin-2020/-/raw/master/ToDoApp-overview.png)
 