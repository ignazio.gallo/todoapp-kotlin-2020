package it.uninsubria.pdm.todoapp

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.util.*
import kotlin.collections.ArrayList

class DatabaseHelper(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        val TAG = "DatabaseHelper"
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 4
        val DATABASE_NAME = "todolist.db"
        // Books table name
        val TABLE_NAME = "todoItems"
        // Books Table Columns names
        val KEY_ID = "id"
        val KEY_TASK = "task"
        val KEY_DATE = "creation_date"
        val KEY_DEADLINE = "deadline_date"
        val KEY_IMAGE_URL = "image_url"

        private val SQL_CREATE_ENTRIES =
            "CREATE TABLE $TABLE_NAME (" +
                    "$KEY_ID integer primary key autoincrement, " +
                    "$KEY_TASK TEXT not null, " +
                    "$KEY_DATE INTEGER, " +
                    "$KEY_DEADLINE INTEGER, " +
                    "$KEY_IMAGE_URL TEXT" +
                    ");"

        private val SQL_SELECT_ALL = "SELECT * FROM $TABLE_NAME"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_NAME"
    }


    @Override
    override fun onCreate(db: SQLiteDatabase) {
        // create new table
        db.execSQL(SQL_CREATE_ENTRIES)
        Log.d(TAG, "onCreate(): create table");
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }


    fun getAllItems(): Collection<TodoItem>? {
        var items: ArrayList<TodoItem> = ArrayList()

        val db = this.writableDatabase

        val cursor: Cursor = db.rawQuery(SQL_SELECT_ALL, null)
        if (cursor.moveToFirst()) {
            do {
                var task = TodoItem()
                task.id = cursor.getString(0).toLong()
                task.todo = cursor.getString(1)
                val createOn = GregorianCalendar()
                createOn.setTimeInMillis(cursor.getLong(2))
                task.createdOn = createOn
                val deadline = GregorianCalendar()
                deadline.setTimeInMillis(cursor.getLong(3))
                task.deadline = deadline
                task.imgUrl = cursor.getString(4)
                items.add(0, task) // Add item to items
            } while (cursor.moveToNext())
        }
        db.close()
        Log.d(TAG, "getAllItems(): $items")
        return items
    }


    fun deleteItem(item: TodoItem) {
        val db = this.writableDatabase
        db.delete(
            TABLE_NAME,
            "$KEY_ID = ?",
            arrayOf(item.id.toString())
        )
        db.close()
        Log.d(TAG, "deleted item $item")
    }

    fun insertItem(item: TodoItem): Long {
        val db = this.writableDatabase

        val values = ContentValues()
        values.put(KEY_TASK, item.todo)
        values.put(KEY_DATE, item.createdOn.timeInMillis)
        values.put(KEY_DEADLINE, item.deadline.timeInMillis)
        values.put(KEY_IMAGE_URL, item.imgUrl)
        item.id = db.insert(TABLE_NAME, null, values)
        if (item.id == -1L)
            throw IllegalArgumentException("Error in SQLite insert")

        db.close()

        Log.d(TAG, "added item $item")
        return item.id
    }
}