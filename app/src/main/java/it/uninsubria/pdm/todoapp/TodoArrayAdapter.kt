package it.uninsubria.pdm.todoapp

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.io.IOException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min


class TodoArrayAdapter(private var activity: Activity, private var items: ArrayList<TodoItem>) :  BaseAdapter(){

    private class ViewHolder(row: View?) {
        var msgTextView: TextView? = null
        var createdData: TextView? = null
        var imgTodo: ImageView? = null
        var deadlineDate: TextView? = null
        var circleProgressBar: CircleProgressBar? = null
        init {
            this.msgTextView = row?.findViewById(R.id.messageTextView)
            this.createdData = row?.findViewById(R.id.createdTextView)
            this.imgTodo = row?.findViewById(R.id.imageView)
            this.deadlineDate = row?.findViewById(R.id.deadlineTextView)
            this.circleProgressBar = row?.findViewById(R.id.circleProgressBar)
        }
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item, null)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var todoItem = items[position]
        viewHolder.msgTextView?.text = todoItem.todo
        viewHolder.createdData?.text = todoItem.dateToString(todoItem.createdOn)
        viewHolder.deadlineDate?.text = todoItem.dateToString(todoItem.deadline)

        viewHolder.imgTodo?.setImageResource(R.mipmap.todo_listview_icon!!)
        val urlImage = todoItem.imgUrl.trim()
        if (urlImage != "") {
            Thread(Runnable {
                try {
                    val url = URL(urlImage)
                    val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                    activity.runOnUiThread(Runnable {
                        viewHolder.imgTodo?.setImageBitmap(bmp)
                    })
                }
                catch (e: IOException) {
                    Log.w("TodoArrayAdapter", "Error in downloading image from URL " + urlImage)
                }

            }).start()
        }

        val nowCal: Calendar = GregorianCalendar()
        val deltaMillis = nowCal.time.time - todoItem.createdOn.time.time
        val intervalMillis = todoItem.deadline.time.time - todoItem.createdOn.time.time
        if (intervalMillis < 0)
            viewHolder.circleProgressBar?.percentage = 1F
        else
            viewHolder.circleProgressBar?.percentage =  max(0f, min(1f, deltaMillis / intervalMillis.toFloat()))
        Log.d("TodoArrayAdapter","emp.deadline: " + SimpleDateFormat("EEEEE dd MMMMM yyyy HH:mm:ss.SSSZ", Locale.ITALIAN).format(todoItem.deadline.getTime()))
        Log.d("TodoArrayAdapter","emp.createdOn: " + SimpleDateFormat("EEEEE dd MMMMM yyyy HH:mm:ss.SSSZ", Locale.ITALIAN).format(todoItem.createdOn.getTime()))

        return view
    }
    override fun getItem(i: Int): TodoItem {
        return items[i]
    }
    override fun getItemId(i: Int): Long {
        return i.toLong()
    }
    override fun getCount(): Int {
        return items.size
    }
}