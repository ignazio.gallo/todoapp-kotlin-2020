package it.uninsubria.pdm.todoapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tool_bar.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.properties.Delegates
import com.google.firebase.auth.ktx.auth


class MainActivity : AppCompatActivity() {
    // the array list containing the Todo items
    var todoItems = ArrayList<TodoItem>()
    var adapter : TodoArrayAdapter by Delegates.notNull()
    val NEW_ITEM_REQUEST = 1

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar)

        // Display icon in the toolbar
        supportActionBar?.setLogo(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayUseLogoEnabled(true)

        val helper = DatabaseHelper(this)
        todoItems.addAll(helper.getAllItems()!!)

        // ArrayAdapter converts an ArrayList of Todo items into View items
        adapter = TodoArrayAdapter(this, todoItems)
        // View items are loaded into the ListView container.
        list_view.setAdapter(adapter)

        // In Kotlin it’s possible to substitute a function that receives an interface with a lambda
        list_view.setOnItemLongClickListener { parent, view, position, id ->
            onLongClickRemoveItem(position)
        }

        // Initialize Firebase Auth
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser == null) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == NEW_ITEM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                val returnValue = data?.getStringExtra("TODO_TASK")
                val deadline = data?.getStringExtra("TODO_DEADLINE")
                val imgUrl = data?.getStringExtra("TODO_IMAGE_URL")
                val df: DateFormat = SimpleDateFormat("dd MM yyyy")
                val date: Date = df.parse(deadline)
                val cal = GregorianCalendar()
                cal.setTime(date)

                Log.d(MainActivity::class.java.name, "onActivityResult() -> $returnValue")
                if (returnValue != null) {
                    addNewItem(returnValue, cal, imgUrl)
                }
                return
            }
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()
        if (id == R.id.action_settings) {
            Toast.makeText(
                applicationContext,
                "Not yet implemented!",
                Toast.LENGTH_SHORT
            ).show()
            return true
        } else if (id == R.id.action_new_item) {
            val i = Intent(applicationContext, AddNewItemActivity::class.java)
            startActivityForResult(i, NEW_ITEM_REQUEST)
            return true
        } else if (id == R.id.action_logout) {
            auth.signOut()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun addNewItem(todo: String, deadline: GregorianCalendar, imgUrl: String?){
        if (todo.length == 0) {
            Toast.makeText(
                applicationContext,
                "Empty ToDo string",
                Toast.LENGTH_LONG
            ).show()
            return
        }
        val newTodo = TodoItem(todo)
        newTodo.deadline = deadline
        if (imgUrl != null && !imgUrl.isEmpty())
            newTodo.imgUrl = imgUrl

        val helper = DatabaseHelper(this)
        helper.insertItem(newTodo)

        todoItems.add(0, newTodo)
        adapter.notifyDataSetChanged()

        Toast.makeText(this, newTodo.toString() + "Added to database", Toast.LENGTH_LONG).show()
    }


    fun onLongClickRemoveItem(position: Int): Boolean {
        // get the item delected for delation
        val item = list_view.getItemAtPosition(position) as TodoItem
        Toast.makeText(
            this, "Deleted Item $position $item",
            Toast.LENGTH_LONG
        ).show()
        // delete the item from the DB
        val helper = DatabaseHelper(this)
        helper.deleteItem(item)
        // delete the item from the listview data structure
        todoItems.removeAt(position)
        // notify the data set has changed to the listview adapter
        adapter.notifyDataSetChanged()
        return true
    }
}
