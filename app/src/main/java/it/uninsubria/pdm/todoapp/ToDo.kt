package it.uninsubria.pdm.todoapp

import java.text.SimpleDateFormat
import java.util.*

class TodoItem() {
    constructor(msg: String) : this(){
        todo = msg
    }

    var id: Long = -1
    var todo: String = ""
    var imgUrl: String = ""
    var createdOn : GregorianCalendar = GregorianCalendar()
    var deadline : GregorianCalendar = GregorianCalendar()

    fun dateToString(date: GregorianCalendar): String {
        return SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(date.getTime())
    }

    override fun toString(): String {
        val currentDate: String = SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN)
            .format(createdOn.getTime())
        return "$currentDate:\n >> $todo"
    }

}